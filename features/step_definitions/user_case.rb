Given(/^I click on "([^"]*)"$/) do |arg1|
  click_on 'Sign In'
end
    
Given(/^I set "([^"]*)" with "([^"]*)"$/) do |field, value|
  fill_in(field, :with => value )
end

Given(/^I should go to LoginPage/) do
  find("h4").visible?
end

Given(/^I click on button "([^"]*)"$/) do |button|
  if button == "Sign In"
    find(:xpath, "//input[@class='btn btn-primary']").click()
  elsif button == "My Tasks"
    find(:xpath, "//a[@class='btn btn-lg btn-success']").click()
  elsif button == "Add task"
    find(:xpath, "//span[@class='input-group-addon glyphicon glyphicon-plus']").click()
  elsif button == "Add Subtask"
    find(:id, "add-subtask").click()
  end
end
  
Then(/^I should see "([^"]*)" in header$/) do |message|
  expect(find(:xpath, "//ul[@class='nav navbar-nav navbar-right']/li[1]/a").text).to eq message
end

Then(/^I should go to "([^"]*)" page$/) do |page|
  if page == "My Tasks"
    expect(current_path).to eq "/tasks"
  end
end

Then(/^I should see "([^"]*)" message$/) do |message|
  expect(find(:xpath, "//h1").text).to eq message
end

Then(/^I should see a new task "([^"]*)"$/) do |title|
  expect(find(:xpath, "//table/tbody/tr[1]/td[2]/a").text).to eq title
end

Given(/^I click on button "([^"]*)" at first grid item$/) do |button|
  find(:xpath, "//table/tbody/tr[1]/td[4]/button").click()
end

Then(/^I should see subtask modal/) do
  find(:xpath, "//div[@class='modal-dialog']").visible?
end

Then(/^I should see a new subtask "([^"]*)"$/) do |title|
  expect(find(:xpath,"//div[@class='modal-body ng-scope']/div[2]/table/tbody/tr[1]/td[2]/a").text).to eq title
end