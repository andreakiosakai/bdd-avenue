Feature: User Case n. 2
	As a ToDo user
	I should be able to create a subtask
	So I can break down my tasks in smaller pieces
	
Scenario: Access Subtask modal
	Given I go to "http://qa-test.avenuecode.com/"
	And I click on "Sign In"
	And I set "user_email" with "andreakiosakai@gmail.com"
	And I set "user_password" with "ABC123teste"
	And I click on button "Sign In"
	And I click on button "My Tasks"
	When I click on button "Manage Subtasks" at first grid item
	Then I should see subtask modal

Scenario: Create SubTask
	Given I go to "http://qa-test.avenuecode.com/"
	And I click on "Sign In"
	And I set "user_email" with "andreakiosakai@gmail.com"
	And I set "user_password" with "ABC123teste"
	And I click on button "Sign In"
	And I click on button "My Tasks"
	And I click on button "Manage Subtasks" at first grid item
	And I set "new_sub_task" with "SubTask"
	And I set "dueDate" with "12/1/2016"
	When I click on button "Add Subtask"
	Then I should see a new subtask "SubTask"