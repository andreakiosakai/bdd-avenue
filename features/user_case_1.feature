Feature: User Case n. 1
  As a ToDo App user
  I should be able to create a task
  So I can manage my tasks

Scenario: Log In
  Given I go to "http://qa-test.avenuecode.com/"
  And I click on "Sign In"
  And I should go to LoginPage
  And I set "user_email" with "andreakiosakai@gmail.com"
  And I set "user_password" with "ABC123teste"
  When I click on button "Sign In"
  Then I should see "Welcome, André Akio Furuko Sakai!" in header
  
Scenario: Access My Tasks Page
  Given I go to "http://qa-test.avenuecode.com/"
  And I click on "Sign In"
  And I set "user_email" with "andreakiosakai@gmail.com"
  And I set "user_password" with "ABC123teste"
  And I click on button "Sign In"
  When I click on button "My Tasks"
  Then I should go to "My Tasks" page
  And I should see "Hey André Akio Furuko Sakai, this is you todo list for today:" message

Scenario: Create a task valid
  Given I go to "http://qa-test.avenuecode.com/"
  And I click on "Sign In"
  And I set "user_email" with "andreakiosakai@gmail.com"
  And I set "user_password" with "ABC123teste"
  And I click on button "Sign In"
  And I click on button "My Tasks"
  And I set "new_task" with "Task number 1"
  When I click on button "Add task"
  Then I should see a new task "Task number 1"