Before do
  
end

Given(/^I go to "([^"]*)"$/) do |site|
  visit site
end

Given(/^I wait "([^"]*)" seconds$/) do |time|
  sleep time.to_i
end